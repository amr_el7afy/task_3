import 'package:flutter/material.dart';
void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'task_3',
    home: home(),
  ));
}
class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  Widget _container(String text1,String image){
    return Padding(padding: EdgeInsets.only(left: 20,right: 20,bottom: 15,),
      child: Container(
        width: 400,
        height: 100,
        decoration: BoxDecoration(
          border: Border.all(width: 1,color: Colors.grey[300],),
          borderRadius: BorderRadius.circular(10)
        ),
       child: Row(
         children: <Widget>[
           Padding( padding:EdgeInsets.only(left: 50,right: 60),child: Text(text1,style: TextStyle(fontSize: 25),),),
           Padding( padding:EdgeInsets.only(left: 70),child: Container(
             width: 70  ,
             height: 70,
             child: Image.asset(image),
             decoration: BoxDecoration(
               color: Colors.grey[200],
               borderRadius: BorderRadius.circular(35)
             ),
           ),)
         ],
       ),
      ),
    );
  }
  Widget _text(String text){
    return Padding(padding: EdgeInsets.only(left: 0,bottom: 20),child:  Text(text,
      style: TextStyle(fontSize: 20),),);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image:DecorationImage(image: AssetImage('assets/logo.png',),fit: BoxFit.cover)
            ),
          width: 150,
          height: 200,
        ),
          _text('من فضلك اختر نوع المستخدم'),
          _text('choose user type'),
          _container('مقدم خدمة', 'assets/111.png'),
          _container('طالب خدمة', 'assets/222.png')

        ],
      ),
    );
  }
}